# Overview
This is a simple script that pulls from the [OpenWeatherMap API](http://openweathermap.org/api) to get weather data. Currently it uses a lot of hard coded values and only supports one location at a time.

# Usage

    weatherTool.py [options]

    Options:
      -h, --help            show this help message and exit
      -c, --current         Fetch current weather data
      -f, --forecast        Fetch forecast weather data
      -d, --dress           Suggest suitable clothing
      -U, --Umbrella        Output ONLY whether or not an umbrella is needed
                            (defaults to 6 hours)
      -u, --umbrella        Output whether or not an umbrella is needed (defaults
                            to 6 hours)
      -F, --Farenheit       Return temperatures in farenheit (default is Celcius)
      -t TIME, --time=TIME  Hours of forecasted weather to consider
      -l LOCATION, --location=LOCATION
                            Loction to obtain weather from. Cities containing
                            spaces should be placed in quotes. If there is
                            ambiguity try placing a regional identifier after the
                            city name (e.g. London,UK or London,ON)
      -g, --graph           Create a temperature graph of the next TIME hours



# To Do
1. Write more tests
2. Switch to using [weather condition IDs](http://openweathermap.org/weather-conditions) instead of values (e.g. use 500 instead of "light rain")
4. Improve documentation
5. Remove depreciated code

#Recent Updates
- Added ability to retrieve current location from `ip-api.com`
    - Hasn't been fully tested but appears to be reasonably accurate. 
    - Generally chooses a location at least nearby.
    - Will only work for direct connections to the internet (i.e. Proxies and VPNs will result in inaccuracies)
- Added graph feature
