#! /usr/bin/python
from urllib2 import Request, urlopen, URLError
from optparse import OptionParser
import json
import sys # currently only used for sys.exit

"""
Plan: be able to specify how long I Plan to go out then script will return how much the temperature will change and if it will begin to rain or snow in that time (this is the reason behind roundToNearestThree)
Ideally be able to specify command line arguments: https://docs.python.org/2/library/optparse.html#handling-boolean-flag-options

TODO: reclassify weather conditions using weather condition ids (http://openweathermap.org/weather-conditions)


POTENTIAL OFF BY ONE ERROR WITH TIME CONVERSIONS

API: http://openweathermap.org/api
"""
Testing = False
MIN_FORECAST = 6 # minimum number of time to consider with forecast
DEFAULT_LOCATION = 'Waterloo,ON'

GRAPH_LENGTH = 20

# classify temperatures by clothing
WINTER_COAT = -5
SWEATER = 0
PANTS = 12

STRONG_WIND_MPH = 25
WIND_ADJUSTMENT = 5 # degree to which strong wind makes apparent temp colder (deg Celcius)
SUN_ADJUSTMENT = 2 # degree to which sun makes apparent temp warmer (deg Celcius)

#parse command line arguments
parser = OptionParser()

parser.add_option("-c", "--current", action="store_true", dest="current", help="Fetch current weather data", default="True")
parser.add_option("-f", "--forecast", action="store_true", dest="forecast", help="Fetch forecast weather data")
parser.add_option("-d", "--dress", action="store_true", dest="dress", help="Suggest suitable clothing")
parser.add_option("-U", "--Umbrella", action="store_true", dest="Umbrella", help="Output ONLY whether or not an umbrella is needed (defaults to 6 hours)") 
parser.add_option("-u", "--umbrella", action="store_true", dest="umbrella", help="Output whether or not an umbrella is needed (defaults to 6 hours)") 
parser.add_option("-F", "--Farenheit", action="store_true", dest="farenheit", help="Return temperatures in farenheit (default is Celcius)")
parser.add_option("-t", "--time", dest="time", help="Hours of forecasted weather to consider", default=MIN_FORECAST) #
parser.add_option("-l", "--location", dest="location", help="Loction to obtain weather from. Cities containing spaces should be placed in quotes. If there is ambiguity try placing a regional identifier after the city name (e.g. London,UK or London,ON)")
parser.add_option("-g", "--graph", action="store_true", dest="graph", help="Create a temperature graph of the next TIME hours")
parser.add_option("-s", "--scale", dest="scale", help="Create a graph using specified min and max temps (usage: -s min:max)")

(options, args) = parser.parse_args()

def roundToBase(n, base=3):
	return int(base * round(float(n)/base))

def getForecastedTemps(data, t): #data must be a json response (this reduces api calls), t must be an integer value for hours in the future
	temps = []
	for i in range(min(len(data["list"]), roundToBase(t)/3)):
		temps.append(data["list"][i]["main"]["temp"])
	return temps

def maxTemperatureIncrease(data, t): #data must be a json response (this reduces api calls), t must be an integer value for hours in the future
	return max(getForecastedTemps(data), roundToBase(t)/3) - data["list"][0]["main"]["temp"]

def maxTemperatureDecrease(data, t): #data must be a json response (this reduces api calls), t must be an integer value for hours in the future
	return -1 * (data["list"][0]["main"]["temp"] - min(getForecastedTemps(data), roundToBase(t)/3))

def convertToCelcius(temp):
	return temp - 273.15

def convertToFarenheit(temp):
	return (temp - 273.15) * 1.8 + 32

def getCurrentTemperature(data): #accepts parsed json data
	return data["main"]["temp"]

def getCurrentConditions(data): #accepts parsed json data
	return data["weather"][0]["main"]

def getCurrentWeather(): #returns parsed json data (depreciated)
	if not Testing:
		request = Request('http://api.openweathermap.org/data/2.5/weather?q=', location)

	try:
		jsonResponse = ""
		if not Testing: #for testing purposes only (reduces api calls)
			response = urlopen(request)
			jsonResponse = response.read()
		else: #delete this block when finished testing
			f = open("sampleCurrentWeatherData.json")
			jsonResponse = f.read()
			f.close()
		
		data = json.loads(jsonResponse)
		return data

	except URLError, e:
		print "HTTP Error code: ", e

def getForecast(): # returns parsed json data (depreciated)
	if not Testing:
		request = Request('http://api.openweathermap.org/data/2.5/forecast?q=Waterloo,ON')

	try:
		jsonResponse = ""
		if not Testing:
			response = urlopen(request)
			jsonResponse = response.read()
		else:
			f = open("SampleForecastData.json", 'r')
			jsonResponse = f.read()
			f.close()

		data = json.loads(jsonResponse)
		return data
		"""
		try:
			data["list"]
			print 'yes'
		except:
			print 'no'


		for i in data["list"]:
			if i["weather"][0]["main"] == "Rain":
				print i["dt_txt"]
		"""
	except URLError, e:
		print "HTTP Error code: ", e

def getWeather(current): # returns parsed json data
	if not Testing:
		if current:
			request = Request('http://api.openweathermap.org/data/2.5/weather?q=' + location)
		else:
			request = Request('http://api.openweathermap.org/data/2.5/forecast?q=' + location)

	try:
		jsonResponse = ""
		if not Testing: #for testing purposes only (reduces api calls)
			response = urlopen(request)
			jsonResponse = response.read()
		else: #delete this block when finished testing
			if current:
				f = open("sampleCurrentWeatherData.json")
			else:
				f = open("SampleForecastData.json", 'r')
			jsonResponse = f.read()
			f.close()
		
		data = json.loads(jsonResponse)
		if data["cod"] == "404":
			print data["message"]
			sys.exit(1)
		return data

	except URLError, e:
		print "HTTP Error code: ", e

def suggestClothing(data, t):
	clothes = {} #using dict so I don't have to remove duplicate values from list
	for i in range(min(len(data["list"]), roundToBase(t)/3)):
		temp = convertToCelcius(data["list"][i]["main"]["temp"])
		conditions = data["list"][i]["weather"][0]["main"]

		if data["list"][i]["wind"]["speed"] > STRONG_WIND_MPH:
			temp = temp - WIND_ADJUSTMENT
		if data["list"][i]["weather"][0]["description"] == "clear sky":
			temp = temp + SUN_ADJUSTMENT

		if temp < -WINTER_COAT:
			clothes["Winter Coat"] = True
		elif temp < SWEATER:
			clothes["Warm Sweater"] = True
		elif temp < PANTS:
			clothes["Pants"] = True
		else:
			clothes["Shorts"] = True

		if conditions in ("Rain", "Thunderstorm"):
			clothes["Umbrella"] = True
		if conditions == "Snow" and not clothes["Winter Coat"]:
			clothes["Jacket"] = True

	return clothes

def getLocation(): # attempts to retrieve current location
	request = Request('http://ip-api.com/json')

	try:
		response = urlopen(request)
		jsonResponse = response.read()
		data = json.loads(jsonResponse)
		if data["city"]:
			return data["city"] + "," + data["region"]
		else:
			return "-1"

	except URLError, e:
		print "HTTP Error code: ", e

def printSpaces(n):
	for i in range(n):
		print " ",

def graphTemp(data, t): #accepts parsed json data and t hours to forecast
	temps = {}
	for i in range(min(len(data["list"]), roundToBase(t)/3)): # place the temperatures into a dictionary
		if options.farenheit:
			temps[data["list"][i]["dt_txt"]] = convertToFarenheit(data["list"][i]["main"]["temp"])
		else:	
			temps[data["list"][i]["dt_txt"]] = convertToCelcius(data["list"][i]["main"]["temp"])

	maxTemp = 0
	minTemp = 0

	if not options.scale:
		maxTemp = temps[data["list"][0]["dt_txt"]]
		minTemp = temps[data["list"][0]["dt_txt"]]
		for i in temps:
			if temps[i] > maxTemp:
				maxTemp = temps[i]
			if temps[i] < minTemp:
				minTemp = temps[i]
	else:
		minTemp = int(options.scale[:options.scale.index(":")])
		maxTemp = int(options.scale[options.scale.index(":") +1:])


	divisionFactor = float((maxTemp - minTemp) / float(GRAPH_LENGTH))
	
	#for i in temps:
	#	temps[i] = int((temps[i] - minTemp) / divisionFactor)

	#print Graph

	printSpaces(10)
	print int(minTemp),
	printSpaces(20)# - len(str(int(minTemp))))
	print int(maxTemp)
 
	for i in temps:
		spaces = GRAPH_LENGTH / 2
		if temps[i] >= minTemp and temps[i] <= maxTemp:
			spaces = int((temps[i] - minTemp) / divisionFactor)
		
		print i + " |",
		printSpaces(spaces)
		if temps[i] >= minTemp and temps[i] <= maxTemp:
			print ".", #eventually want to use 
		else:
			print " ",
		printSpaces(20 - spaces)
		print "| ",
		print temps[i]

def needUmbrella(data, t): #accepts parsed json data and t hours to forecast
	for i in range(min(len(data["list"]), roundToBase(t)/3)):
		if data["list"][i]["weather"][0]["main"] == "Rain":
			return True
	return False

def test():
	#asserting actual value = 9.44 failed. Returned 9.439999999999998 for some reason
	assert str(getCurrentWeather()) == "9.44", "getCurrentWeather returned %s" % str(getCurrentWeather())



#Options Logic
if not options.location:
	location = getLocation()
	if location == "-1":
		location = DEFAULT_LOCATION
else:
	location = options.location
	location = location.replace(" ", '_')

if options.Umbrella or options.umbrella:
	if needUmbrella(getWeather(False), options.time):
		print "Umbrella"
	if options.Umbrella:
		sys.exit(0)

if options.current:
	
	data = getWeather(True)
	
	temp =  getCurrentTemperature(data)
	if not options.farenheit:
		print convertToCelcius(temp), 'C'
	else:
		print convertToFarenheit(temp), 'F'
	print getCurrentConditions(data)

if options.forecast:
	data = getWeather(False)
	temps = getForecastedTemps(data, options.time)
	current = data["list"][0]["main"]["temp"]
	maximum = max(temps)
	minimum = min(temps)

	#QUESTION: would it be beneficial to know when the high and low temperatures are?
	if not options.farenheit:
		print "Current: ", convertToCelcius(current)
		print "High: ", convertToCelcius(maximum)
		print "Low: ", convertToCelcius(minimum)
	else:
		print "Current: ", convertToFarenheit(current)
		print "High: ", convertToFarenheit(maximum)
		print "Low: ", convertToFarenheit(minimum)

	for i in range(min(len(data["list"]), roundToBase(options.time)/3)):
		if data["list"][i]["weather"][0]["main"] == "Rain":
			print "Next rain at: ", data["list"][i]["dt_txt"]
			break

if options.dress:
	clothes = suggestClothing(getWeather(False), options.time)
	print "Disclaimer: these are suggested clothing options and should not be followed blindly"
	for k in clothes:
		print k,   
	print ""

if options.graph:
	graphTemp(getWeather(False), options.time)